#include <iostream>

#include <QApplication>
#include <QCoreApplication>
#include <QString>

#include "MainWindow.hpp"
#include "Version.hpp"

int main(int argc, char* argv[])
{
    Q_INIT_RESOURCE(resources);

    QApplication app(argc, argv);
    QCoreApplication::setOrganizationName("EduceLab");
    QCoreApplication::setApplicationName(
        QString::fromStdString(ProjectInfo::Name()));
    QCoreApplication::setApplicationVersion(
        QString::fromStdString(ProjectInfo::VersionString()));

    MainWindow window;
    window.show();

    return app.exec();
}