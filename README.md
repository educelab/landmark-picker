# Landmarks Picker
A GUI for selecting matching landmark pairs in images. For use with 
[registration-toolkit](https://gitlab.com/educelab/registration-toolkit).

![Landmarks Picker](graphics/screenshot.png)

## Installation
### Using Homebrew
We provide pre-built binaries for our tools through our
[Homebrew Casks tap](https://github.com/educelab/homebrew-casks):

```shell
brew install --no-quarantine educelab/casks/landmarks-picker
```

Our binaries are signed with a generic signature and thus do not pass macOS
Gatekeeper on Apple Silicon devices without explicit approval. We suggest 
installing with Homebrew's `--no-quarantine` flag.
The `Landmarks Picker.app` GUI will be installed to `/Applications/`.

### From source
#### Requirements
- CMake 3.24+
- C++17 compiler
- Qt 6.5+

#### Build and install
```shell
# Get the source coude
git clone https://gitlab.com/educelab/landmark-picker.git
cd landmark-picker

# Build the app
cmake -DCMAKE_BUILD_TYPE=Release -S . -B build
cmake --build build/

# OPTIONAL: Install to the system
cmake --install build/
```