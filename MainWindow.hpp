#pragma once

#include <QFileDialog>
#include <QItemSelection>
#include <QLabel>
#include <QMainWindow>
#include <QMessageBox>
#include <QPointer>
#include <QSplitter>
#include <QStandardItemModel>
#include <QTableView>
#include <QWidget>

#include "DockManager.h"

#include "ImageViewer.hpp"
#include "SettingsWindow.hpp"

class MainWindow final : public QMainWindow
{
    Q_OBJECT

public:
    enum class Mode { None, Editing };
    explicit MainWindow();

public slots:
    void OnLoadFixed() const;
    void OnLoadMoving() const;

    void OnFixedClick(QPointF pos) const;
    void OnMovingClick(QPointF pos) const;

    void OnAddLandmark() const;
    void OnRemoveLandmark() const;
    void OnSelectLandmark(
        const QItemSelection& s, const QItemSelection& /* unused */) const;
    void OnNewLandmarksList() const;
    void OnSaveLandmarksList() const;
    void OnLoadLandmarksList();

protected:
    void closeEvent(QCloseEvent* event) override;

private:
    void save_settings_() const;
    void load_settings_();

    SettingsWindow* settings_;

    ads::CDockManager* dockMgr_;

    ImageViewer* fixedViewer_;
    ImageViewer* movingViewer_;

    Mode mode_{Mode::None};
    QTableView* ldmView_;
    QStandardItemModel* landmarks_;

    QGraphicsEllipseItem* fixedLdm_;
    QGraphicsEllipseItem* movingLdm_;

    [[nodiscard]] auto get_file_open_path_(const QString& filter) const
        -> QString;
    [[nodiscard]] auto get_ldm_save_path_() const -> QString;
    QMessageBox* imgLoaderMsgBox_;
    QFileDialog* imgPicker_;
    QFileDialog* ldmPicker_;
};



