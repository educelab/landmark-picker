#include "MainWindow.hpp"

#include <QAction>
#include <QCloseEvent>
#include <QFileDialog>
#include <QGraphicsPixmapItem>
#include <QGridLayout>
#include <QMenuBar>
#include <QRegularExpression>
#include <QShortcut>
#include <QTableView>
#include <QToolBar>
#include <QtCore>

#include <filesystem>
#include <iostream>

#include "DockAreaWidget.h"
#include "DockSplitter.h"
#include "Icons.hpp"

namespace fs = std::filesystem;

float LDM_SIZE = 10;

const QString FILTER_IMG = "Image Files (*.png *.jpg *.tif *.tiff)";
const QString FILTER_LDM = "Landmarks Files (*.ldm)";

class MinTableView final : public QTableView
{
public:
    using QTableView::QTableView;
    [[nodiscard]] auto minimumSizeHint() const -> QSize override
    {
        return {425, 100};
    }
};

MainWindow::MainWindow()
{
    // Set the title bar
    setWindowTitle(tr("Landmarks Picker"));

    // Set the window icon
    setWindowIcon(QIcon{":/icons/app"});
    resize(1280, 720);

    auto* fileMenu = menuBar()->addMenu("&File");
    auto* viewMenu = menuBar()->addMenu("&View");
    auto* windowMenu = viewMenu->addMenu("&Windows");

    // Setup dock manager
    ads::CDockManager::setConfigFlag(
        ads::CDockManager::EqualSplitOnInsertion, true);
    dockMgr_ = new ads::CDockManager(this);

    // Setup image viewers
    fixedViewer_ = new ImageViewer(this);
    movingViewer_ = new ImageViewer(this);

    connect(
        fixedViewer_, &ImageViewer::mouseClick, this,
        &MainWindow::OnFixedClick);
    connect(
        movingViewer_, &ImageViewer::mouseClick, this,
        &MainWindow::OnMovingClick);

    // Add circle placeholders
    QPen pen(Qt::green, 1, Qt::SolidLine);
    fixedLdm_ = new QGraphicsEllipseItem();
    fixedLdm_->setVisible(false);
    fixedLdm_->setPen(pen);
    fixedLdm_->setZValue(1);
    fixedLdm_->setRect({-LDM_SIZE / 2, -LDM_SIZE / 2, LDM_SIZE, LDM_SIZE});
    movingLdm_ = new QGraphicsEllipseItem();
    movingLdm_->setVisible(false);
    movingLdm_->setPen(pen);
    movingLdm_->setZValue(1);
    movingLdm_->setRect({-LDM_SIZE / 2, -LDM_SIZE / 2, LDM_SIZE, LDM_SIZE});
    fixedViewer_->scene()->addItem(fixedLdm_);
    movingViewer_->scene()->addItem(movingLdm_);

    // InfoPanel
    auto* infoPanel = new QFrame();

    ldmView_ = new MinTableView();
    ldmView_->setSelectionBehavior(QTableView::SelectionBehavior::SelectRows);
    ldmView_->setSelectionMode(QTableView::SelectionMode::ExtendedSelection);
    ldmView_->setDragDropMode(QTableView::DragDropMode::NoDragDrop);
    landmarks_ = new QStandardItemModel(0, 4);
    landmarks_->setHorizontalHeaderLabels({"fX", "fY", "mX", "mY"});
    ldmView_->setModel(landmarks_);
    ldmView_->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    // landmark control buttons
    // Setup buttons child widget
    int size = style()->pixelMetric(QStyle::PM_ButtonIconSize);
    QSize iconSize(size, size);

    auto ldmAdd = new QAction(lp::GetThemedIcon("add"), "Add landmark");
    auto ldmRemove =
        new QAction(lp::GetThemedIcon("remove"), "Remove landmark");
    auto ldmNewList =
        new QAction(lp::GetThemedIcon("new"), "New landmarks list");
    auto ldmSave =
        new QAction(lp::GetThemedIcon("save"), "Save landmarks list");
    auto ldmLoad =
        new QAction(lp::GetThemedIcon("load"), "Load landmarks list");

    connect(ldmAdd, &QAction::triggered, this, &MainWindow::OnAddLandmark);
    connect(
        ldmRemove, &QAction::triggered, this, &MainWindow::OnRemoveLandmark);
    connect(
        ldmView_->selectionModel(), &QItemSelectionModel::selectionChanged,
        this, &MainWindow::OnSelectLandmark);
    connect(
        ldmNewList, &QAction::triggered, this, &MainWindow::OnNewLandmarksList);
    connect(
        ldmLoad, &QAction::triggered, this, &MainWindow::OnLoadLandmarksList);
    connect(
        ldmSave, &QAction::triggered, this, &MainWindow::OnSaveLandmarksList);

    auto deleteShortcut =
        new QShortcut(QKeySequence(Qt::Key_Backspace), ldmView_);
    connect(
        deleteShortcut, &QShortcut::activated, this,
        &MainWindow::OnRemoveLandmark);

    // Add them to the widget in order
    auto toolbar = new QToolBar("Landmarks Tools");
    toolbar->addAction(ldmAdd);
    toolbar->addAction(ldmRemove);
    toolbar->addSeparator();
    toolbar->addAction(ldmNewList);
    toolbar->addAction(ldmLoad);
    toolbar->addAction(ldmSave);
    toolbar->setIconSize(iconSize);
    toolbar->setFloatable(false);
    toolbar->setMovable(false);

    auto ldmLayout = new QVBoxLayout;
    ldmLayout->addWidget(ldmView_);
    ldmLayout->addWidget(toolbar);
    infoPanel->setLayout(ldmLayout);

    auto* fixedDocked = new ads::CDockWidget("Fixed Image");
    fixedDocked->setWidget(fixedViewer_);
    windowMenu->addAction(fixedDocked->toggleViewAction());
    auto* viewArea =
        dockMgr_->addDockWidget(ads::RightDockWidgetArea, fixedDocked);

    auto* movingDocked = new ads::CDockWidget("Moving Image");
    movingDocked->setWidget(movingViewer_);
    windowMenu->addAction(movingDocked->toggleViewAction());
    auto* movingArea = dockMgr_->addDockWidget(
        ads::RightDockWidgetArea, movingDocked, viewArea);

    // Add widgets to docking interface
    auto* infoDocked = new ads::CDockWidget("Landmarks");
    infoDocked->setWidget(infoPanel);
    windowMenu->addAction(infoDocked->toggleViewAction());
    dockMgr_->addDockWidget(ads::LeftDockWidgetArea, infoDocked, viewArea);

    // Settings
    settings_ = new SettingsWindow(this);

    // Menu
    fileMenu->addAction("Open fixed...", this, &MainWindow::OnLoadFixed);
    fileMenu->addAction("Open moving...", this, &MainWindow::OnLoadMoving);
    fileMenu->addSeparator();
    auto prefsAction = new QAction("&Preferences");
    prefsAction->setStatusTip("Open the preferences menu");
    connect(prefsAction, &QAction::triggered, settings_, &SettingsWindow::show);
    fileMenu->addAction(prefsAction);

    // Image loader
    imgLoaderMsgBox_ = new QMessageBox(this);
    imgLoaderMsgBox_->setIcon(QMessageBox::Critical);
    imgLoaderMsgBox_->setWindowTitle("Landmarks Picker");
    imgLoaderMsgBox_->setText("Failed to load image.");
    imgLoaderMsgBox_->setInformativeText(
        "The selected image may be corrupt or unreadable. If loading a large "
        "image, you may consider increasing the \'Image allocation limit\' in "
        "Preferences.");

    imgPicker_ = new QFileDialog(this);
    imgPicker_->setFileMode(QFileDialog::ExistingFile);
    imgPicker_->setDirectory(
        QStandardPaths::writableLocation(QStandardPaths::HomeLocation));
    imgPicker_->setNameFilter(FILTER_IMG);
    ldmPicker_ = new QFileDialog(this);
    ldmPicker_->setFileMode(QFileDialog::ExistingFile);
    ldmPicker_->setDirectory(imgPicker_->directory());
    ldmPicker_->setNameFilter(FILTER_LDM);
    ldmPicker_->setDefaultSuffix("ldm");

    // Load saved settings
    load_settings_();
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    save_settings_();
    event->accept();
}

void MainWindow::OnFixedClick(QPointF pos) const
{
    if (!ldmView_->selectionModel()->hasSelection()) {
        return;
    }

    const auto row = ldmView_->selectionModel()->selectedRows().at(0).row();
    landmarks_->setData(landmarks_->index(row, 0), pos.x());
    landmarks_->setData(landmarks_->index(row, 1), pos.y());

    fixedLdm_->setPos(pos);
    fixedLdm_->setVisible(true);
}

void MainWindow::OnMovingClick(QPointF pos) const
{
    if (!ldmView_->selectionModel()->hasSelection()) {
        return;
    }

    const auto row = ldmView_->selectionModel()->selectedRows().at(0).row();
    landmarks_->setData(landmarks_->index(row, 2), pos.x());
    landmarks_->setData(landmarks_->index(row, 3), pos.y());

    movingLdm_->setPos(pos);
    movingLdm_->setVisible(true);
}

void MainWindow::OnSelectLandmark(
    const QItemSelection& s, const QItemSelection& /* unused */) const
{
    if (!ldmView_->selectionModel()->hasSelection() || s.isEmpty()) {
        fixedLdm_->setVisible(false);
        movingLdm_->setVisible(false);
        return;
    }

    const auto fX = s.indexes()[0].data().toReal();
    const auto fY = s.indexes()[1].data().toReal();
    const auto mX = s.indexes()[2].data().toReal();
    const auto mY = s.indexes()[3].data().toReal();

    fixedLdm_->setPos(fX, fY);
    movingLdm_->setPos(mX, mY);
    fixedLdm_->setVisible(true);
    movingLdm_->setVisible(true);

    fixedViewer_->view()->centerOn(fixedLdm_);
    movingViewer_->view()->centerOn(movingLdm_);
}

void MainWindow::OnAddLandmark() const
{
    auto fixedPos = fixedViewer_->view()->mapToScene(
        fixedViewer_->view()->viewport()->rect().center());
    auto movingPos = movingViewer_->view()->mapToScene(
        movingViewer_->view()->viewport()->rect().center());

    landmarks_->appendRow({});
    auto row = landmarks_->rowCount() - 1;
    landmarks_->setData(landmarks_->index(row, 0), fixedPos.x());
    landmarks_->setData(landmarks_->index(row, 1), fixedPos.y());
    landmarks_->setData(landmarks_->index(row, 2), movingPos.x());
    landmarks_->setData(landmarks_->index(row, 3), movingPos.y());
    ldmView_->selectRow(row);
}

void MainWindow::OnRemoveLandmark() const
{
    if (!ldmView_->selectionModel()->hasSelection()) {
        return;
    }

    auto selected = ldmView_->selectionModel()->selectedRows();
    ldmView_->clearSelection();
    ldmView_->setSelectionMode(QTableView::SelectionMode::NoSelection);
    std::sort(
        selected.begin(), selected.end(),
        [](const QModelIndex& l, QModelIndex& r) { return l.row() > r.row(); });
    for (auto r : selected) {
        landmarks_->removeRow(r.row());
    }
    ldmView_->setSelectionMode(QTableView::SelectionMode::ExtendedSelection);

    if (landmarks_->rowCount() == 0) {
        fixedLdm_->setVisible(false);
        movingLdm_->setVisible(false);
    } else {
        auto row = std::prev(selected.end(), 1)->row() - 1;
        row = (row < 0) ? 0 : row;
        ldmView_->selectRow(row);
    }
}

void MainWindow::OnNewLandmarksList() const
{
    landmarks_->setRowCount(0);
    OnAddLandmark();
}

void MainWindow::OnSaveLandmarksList() const
{
    const auto path = get_ldm_save_path_();
    if (path.isEmpty()) {
        return;
    }
    QSettings settings;
    const auto parent = fs::path(path.toStdString()).parent_path().string();
    settings.setValue("main/pickerDir", QString::fromStdString(parent));
    imgPicker_->setDirectory(QString::fromStdString(parent));

    QFile file(path);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return;
    }

    QTextStream out(&file);
    for (int r = 0; r < landmarks_->rowCount(); r++) {
        out << landmarks_->data(landmarks_->index(r, 0)).toFloat() << " ";
        out << landmarks_->data(landmarks_->index(r, 1)).toFloat() << " ";
        out << landmarks_->data(landmarks_->index(r, 2)).toFloat() << " ";
        out << landmarks_->data(landmarks_->index(r, 3)).toFloat();
        out << "\n";
    }

    file.close();
}

void MainWindow::OnLoadLandmarksList()
{
    // Select a file
    const auto path = get_file_open_path_(FILTER_LDM);
    if (path.isEmpty()) {
        return;
    }
    QSettings settings;
    const auto parent = fs::path(path.toStdString()).parent_path().string();
    settings.setValue("main/pickerDir", QString::fromStdString(parent));
    imgPicker_->setDirectory(QString::fromStdString(parent));

    QFile file(path);
    if (not file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }

    // Create a new table and parse each line
    QTextStream in(&file);
    auto* loaded = new QStandardItemModel(0, 4);
    while (!in.atEnd()) {
        auto line = in.readLine();
        auto vals = line.split(QRegularExpression("\\s+"), Qt::SkipEmptyParts);

        if (vals.size() != 4) {
            std::cerr << "Failed to load landmarks file" << std::endl;
            return;
        }

        loaded->appendRow({});
        const auto row = loaded->rowCount() - 1;
        loaded->setData(loaded->index(row, 0), vals.at(0).toFloat());
        loaded->setData(loaded->index(row, 1), vals.at(1).toFloat());
        loaded->setData(loaded->index(row, 2), vals.at(2).toFloat());
        loaded->setData(loaded->index(row, 3), vals.at(3).toFloat());
    }
    file.close();

    // Update the view and pointers
    landmarks_->disconnect();
    landmarks_ = loaded;
    landmarks_->setHorizontalHeaderLabels({"fX", "fY", "mX", "mY"});
    ldmView_->setModel(landmarks_);
    connect(
        ldmView_->selectionModel(), &QItemSelectionModel::selectionChanged,
        this, &MainWindow::OnSelectLandmark);
}

void MainWindow::OnLoadFixed() const
{
    const auto path = get_file_open_path_(FILTER_IMG);
    if (path.isEmpty()) {
        return;
    }
    QSettings settings;
    const auto parent = fs::path(path.toStdString()).parent_path().string();
    settings.setValue("main/pickerDir", QString::fromStdString(parent));
    ldmPicker_->setDirectory(QString::fromStdString(parent));

    if (const QPixmap i(path); i.isNull()) {
        imgLoaderMsgBox_->exec();
    } else {
        fixedViewer_->setImage(i, true);
    }
}

void MainWindow::OnLoadMoving() const
{
    const auto path = get_file_open_path_(FILTER_IMG);
    if (path.isEmpty()) {
        return;
    }
    QSettings settings;
    const auto parent = fs::path(path.toStdString()).parent_path().string();
    settings.setValue("main/pickerDir", QString::fromStdString(parent));
    ldmPicker_->setDirectory(QString::fromStdString(parent));

    if (const QPixmap i(path); i.isNull()) {
        imgLoaderMsgBox_->exec();
    } else {
        movingViewer_->setImage(i, true);
    }
}

auto MainWindow::get_file_open_path_(const QString& filter) const -> QString
{
    QFileDialog* picker = (filter == FILTER_IMG) ? imgPicker_ : ldmPicker_;
    picker->setAcceptMode(QFileDialog::AcceptOpen);
    picker->setFileMode(QFileDialog::ExistingFile);
    if (picker->exec()) {
        auto names = picker->selectedFiles();
        return names[0];
    }
    return {};
}

auto MainWindow::get_ldm_save_path_() const -> QString
{
    ldmPicker_->setAcceptMode(QFileDialog::AcceptSave);
    ldmPicker_->setFileMode(QFileDialog::AnyFile);
    if (ldmPicker_->exec()) {
        auto names = ldmPicker_->selectedFiles();
        return names[0];
    }
    return {};
}

void MainWindow::save_settings_() const
{
    QSettings settings;
    settings.setValue("main/geometry", saveGeometry());
    settings.setValue("main/state", saveState());
    settings.setValue("main/dock/geometry", dockMgr_->saveGeometry());
    settings.setValue("main/dock/state", dockMgr_->saveState());
}

void MainWindow::load_settings_()
{
    QSettings settings;
    if (settings.contains("main/geometry")) {
        restoreGeometry(settings.value("main/geometry").toByteArray());
    } else if (settings.contains("mainWin/geometry")) {
        restoreGeometry(settings.value("mainWin/geometry").toByteArray());
    }
    if (settings.contains("main/state")) {
        restoreState(settings.value("main/state").toByteArray());
    } else if (settings.contains("mainWin/state")) {
        restoreState(settings.value("mainWin/state").toByteArray());
    }
    if (settings.contains("main/dock/geometry")) {
        dockMgr_->restoreGeometry(
            settings.value("main/dock/geometry").toByteArray());
    }
    if (settings.contains("main/dock/state")) {
        dockMgr_->restoreState(settings.value("main/dock/state").toByteArray());
    }
    if (settings.contains("main/pickerDir")) {
        const auto dir = settings.value("main/pickerDir").toString();
        imgPicker_->setDirectory(dir);
        ldmPicker_->setDirectory(dir);
    }
}