#include "Icons.hpp"

#include <QLabel>
#include <QtCore>

#include <iostream>
#include <optional>
#include <regex>
#include <string>
#include <unordered_map>

namespace
{
using IconMap = std::unordered_map<std::string, QIcon>;
std::optional<IconMap> icons;

auto IsDarkMode() -> bool
{
    const QLabel l{"TEST"};
    const auto fg = l.palette().color(QPalette::WindowText).value();
    const auto bg = l.palette().color(QPalette::Window).value();
    return fg > bg;
}

void LoadIcons()
{
    icons = IconMap();
    const auto iconRegex = std::regex(
        R"(^(.+)_(normal|active|disabled|selected)_(\d+)x(\d+)(@\d+x)*$)");

    const auto iconDir = QDir(":/icons/");
    const auto iconFiles = iconDir.entryList();

    for (const auto& file : iconFiles) {
        std::smatch groups;
        if (const auto f = file.toStdString();
            std::regex_match(f, groups, iconRegex)) {
            if (groups[5].length() != 0) {
                // Skip High DPI files. QImageReader picks them up by default.
                continue;
            }

            // Load/insert QIcon from map
            std::string key = groups[1];
            QIcon icon = icons.value()[key];

            // Parse size
            const int w = std::stoi(groups[3]);
            const int h = std::stoi(groups[4]);
            QSize size{w, h};

            // Parse mode
            QIcon::Mode mode{QIcon::Normal};
            if (groups[2] == "normal") {
                mode = QIcon::Normal;
            } else if (groups[2] == "active") {
                mode = QIcon::Active;
            } else if (groups[2] == "disabled") {
                mode = QIcon::Disabled;
            } else if (groups[2] == "selected") {
                mode = QIcon::Selected;
            }

            // Load icon and put back in the map
            icon.addFile(":icons/" + file, size, mode);
            icons.value()[key] = icon;
        }
    }
}

}  // namespace

namespace lp
{

auto IconsList() -> std::vector<std::string>
{
    if (not icons) {
        LoadIcons();
    }

    std::vector<std::string> keys;
    for (const auto& [key, _] : icons.value()) {
        keys.emplace_back(key);
    }
    return keys;
}

auto GetIcon(const std::string_view key) -> QIcon
{
    if (not icons) {
        LoadIcons();
    }
    const auto icon = icons.value().find(std::string(key));
    if (icon == icons.value().end()) {
        return {};
    }
    return icon->second;
}

auto GetThemedIcon(const std::string_view key) -> QIcon
{
    if (IsDarkMode()) {
        return GetIcon(std::string(key) + "_dark");
    }
    return GetIcon(std::string(key) + "_light");
}

}  // namespace lp
