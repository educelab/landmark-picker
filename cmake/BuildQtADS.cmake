FetchContent_Declare(
    QtADS
    GIT_REPOSITORY https://github.com/githubuser0xFFFF/Qt-Advanced-Docking-System.git
    GIT_TAG 4.3.0
    EXCLUDE_FROM_ALL
)
set(BUILD_EXAMPLE OFF CACHE INTERNAL "")
set(BUILD_STATIC ON CACHE INTERNAL "")
FetchContent_MakeAvailable(QtADS)