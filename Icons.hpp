#pragma once

#include <string>
#include <string_view>
#include <vector>

#include <QIcon>

namespace lp
{

auto IconsList() -> std::vector<std::string>;

auto GetIcon(std::string_view key) -> QIcon;

auto GetThemedIcon(std::string_view key) -> QIcon;
}  // namespace lp
