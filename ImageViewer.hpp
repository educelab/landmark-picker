#pragma once

#include <QFrame>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMouseEvent>
#include <QWheelEvent>

class ZoomGraphicsView : public QGraphicsView
{
    Q_OBJECT
public:
    ZoomGraphicsView(QWidget* parent = nullptr);
    void enableZoom(bool enable);
    [[nodiscard]] auto zoomEnabled() const -> bool;
    void wheelEvent(QWheelEvent* event) override;

private:
    bool zoomEnabled_{true};
};

class ClickableGraphicsScene : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit ClickableGraphicsScene(QObject* parent = nullptr);

protected:
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* e) override;

signals:
    void mouseClick(QPointF pos);
};

class ImageViewer : public QFrame
{
    Q_OBJECT
public:
    ImageViewer(QWidget* parent = nullptr);
    void mouseDoubleClickEvent(QMouseEvent* event) override;
    void setImage(const QPixmap& image, bool refit = false);
    void enableZoom(bool enable) const;
    void zoomFit() const;

    auto scene() const -> QGraphicsScene*;
    auto view() const -> QGraphicsView*;

signals:
    void mouseClick(QPointF pos);

private:
    ZoomGraphicsView* viewport_;
    ClickableGraphicsScene* scene_;
    QGraphicsPixmapItem* pixmapItem_;
};
