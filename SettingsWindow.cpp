#include "SettingsWindow.hpp"

#include <QCloseEvent>
#include <QGridLayout>
#include <QGroupBox>
#include <QImageReader>
#include <QLabel>
#include <QPushButton>
#include <QSettings>
#include <QSizePolicy>
#include <QSpinBox>
#include <QVBoxLayout>

using SizePolicy = QSizePolicy::Policy;

SettingsWindow::SettingsWindow(QWidget* parent, Qt::WindowFlags f)
    : QDialog{parent, f}
{
    setModal(true);
    setWindowTitle("Landmarks Picker");
    setSizePolicy(SizePolicy::Fixed, SizePolicy::Fixed);
    setLayout(new QVBoxLayout());

    auto prefsGroup = new QGroupBox(this);
    prefsGroup->setTitle("Preferences");
    auto prefsLayout = new QGridLayout();
    prefsGroup->setLayout(prefsLayout);
    layout()->addWidget(prefsGroup);

    img_alloc_limit_ = new QSpinBox(this);
    img_alloc_limit_->setMinimum(0);
    img_alloc_limit_->setMaximum(1024);
    img_alloc_limit_->setValue(4096);
    img_alloc_limit_->setSuffix("MB");
    img_alloc_limit_->setToolTip(
        "<p>Allocation limit for images in MBs. Set to 0 to disable the "
        "allocation limit.</p>");
    prefsLayout->addWidget(
        new QLabel("Image allocation limit:"), 0, 0, Qt::AlignLeft);
    prefsLayout->addWidget(img_alloc_limit_, 0, 1, Qt::AlignLeft);

    // Button box
    btn_box_ = new QDialogButtonBox(
        QDialogButtonBox::Save | QDialogButtonBox::Cancel |
            QDialogButtonBox::RestoreDefaults,
        this);
    layout()->addWidget(btn_box_);
    connect(
        btn_box_, &QDialogButtonBox::accepted, this,
        &SettingsWindow::closeAndSave_);
    connect(
        btn_box_, &QDialogButtonBox::rejected, this,
        &SettingsWindow::closeAndReset_);
    auto restoreBtn = btn_box_->button(QDialogButtonBox::RestoreDefaults);
    connect(
        restoreBtn, &QPushButton::clicked, this,
        &SettingsWindow::resetSettings_);

    loadSettings_();
}

void SettingsWindow::closeEvent(QCloseEvent* event)
{
    closeAndReset_();
    QDialog::closeEvent(event);
}

void SettingsWindow::closeAndSave_()
{
    saveSettings_();
    this->accept();
}

void SettingsWindow::closeAndReset_()
{
    loadSettings_();
    this->reject();
}

void SettingsWindow::saveSettings_()
{
    QSettings settings;
    QImageReader::setAllocationLimit(img_alloc_limit_->value());
    settings.setValue("img_alloc_limit", img_alloc_limit_->value());
}

void SettingsWindow::loadSettings_()
{
    QSettings settings;
    auto allocLimit = settings.value("img_alloc_limit", 1024).toInt();
    QImageReader::setAllocationLimit(allocLimit);
    img_alloc_limit_->setValue(allocLimit);
}

void SettingsWindow::resetSettings_() { img_alloc_limit_->setValue(1024); }
