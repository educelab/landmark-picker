#include "ImageViewer.hpp"

#include <cmath>

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <QVBoxLayout>

///// ZoomGraphicsView /////
ZoomGraphicsView::ZoomGraphicsView(QWidget* parent) : QGraphicsView(parent)
{
    this->setTransformationAnchor(AnchorUnderMouse);
}

void ZoomGraphicsView::enableZoom(const bool enable) { zoomEnabled_ = enable; }

auto ZoomGraphicsView::zoomEnabled() const -> bool { return zoomEnabled_; }

void ZoomGraphicsView::wheelEvent(QWheelEvent* event)
{
    if (zoomEnabled_) {
        const auto angle = event->angleDelta().y();
        const auto factor = std::pow(1.0015F, static_cast<float>(angle));
        this->scale(factor, factor);
    }
}

///// ClickableGraphicsScene /////
ClickableGraphicsScene::ClickableGraphicsScene(QObject* parent)
    : QGraphicsScene(parent)
{
}

void ClickableGraphicsScene::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* e)
{
    emit mouseClick(e->scenePos());
    QGraphicsScene::mousePressEvent(e);
}

///// ImageViewer /////
ImageViewer::ImageViewer(QWidget* parent) : QFrame(parent)
{
    viewport_ = new ZoomGraphicsView();
    viewport_->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    viewport_->setRenderHints(
        QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    viewport_->setDragMode(QGraphicsView::ScrollHandDrag);
    viewport_->enableZoom(true);

    scene_ = new ClickableGraphicsScene(viewport_);
    // TODO: This conflicts with the mouse drag event
    connect(
        scene_, &ClickableGraphicsScene::mouseClick, this,
        &ImageViewer::mouseClick);
    viewport_->setScene(scene_);

    pixmapItem_ = scene_->addPixmap(QPixmap());

    auto* layout = new QVBoxLayout();
    layout->addWidget(viewport_);
    this->setLayout(layout);

    this->setSizePolicy(
        QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
}

void ImageViewer::mouseDoubleClickEvent(QMouseEvent* event)
{
    // Disabled because landmark editing uses double-click
    // if (viewport_->zoomEnabled()) {
    //     this->zoomFit();
    // }
}

void ImageViewer::setImage(const QPixmap& image, const bool refit)
{
    if (refit) {
        scene_->removeItem(pixmapItem_);
        pixmapItem_ = scene_->addPixmap(image);
        // pixmapItem_->setTransformationMode(Qt::SmoothTransformation);
        // const auto halfW = static_cast<float>(image.width()) / 2.f;
        // const auto halfH = static_cast<float>(image.height()) / 2.f;
        // pixmapItem_->setOffset(-halfW, -halfH);
        zoomFit();
    } else {
        pixmapItem_->setPixmap(image);
    }
}

void ImageViewer::enableZoom(const bool enable) const
{
    viewport_->enableZoom(enable);
}

void ImageViewer::zoomFit() const
{
    viewport_->fitInView(pixmapItem_, Qt::KeepAspectRatio);
}

auto ImageViewer::scene() const -> QGraphicsScene* { return scene_; }

auto ImageViewer::view() const -> QGraphicsView* { return viewport_; }
