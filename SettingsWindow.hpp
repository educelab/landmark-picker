#pragma once

#include <QDialog>
#include <QDialogButtonBox>
#include <QPointer>
#include <QSpinBox>

class SettingsWindow : public QDialog
{
    Q_OBJECT
public:
    explicit SettingsWindow(
        QWidget* parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags());

protected:
    void closeEvent(QCloseEvent* event) override;

private:
    void closeAndSave_();
    void closeAndReset_();

    void saveSettings_();
    void loadSettings_();
    void resetSettings_();

    QPointer<QSpinBox> img_alloc_limit_;
    QPointer<QDialogButtonBox> btn_box_;
};